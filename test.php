<?php
require_once "enviroment.php";
require_once "dba.php";

/**
 * init
 */
$enviroment = new enviroment();
$phptime = time();

// $sql = file_get_contents('/othertemplates/featuredevent.sql');

// $promotedevent = dba::prepareExecuteGetStatement($sql);

/**
 * get templates from file
 * access via $templates['name']
 */
$htmltemplates = json_decode(file_get_contents('templates/html/index.json'));
$templates = [];
foreach ($htmltemplates as $template => $filename) {
    $templates[$template] = file_get_contents('templates/html/' . $filename);
};

/**
 * generate main menu entries from DB
 * and put them in main template
 */
// $sql = "";
// $menuelems = dba::prepareExecuteGetStatement($sql);
$menuelems = ['mainmenu elem1','mainmenu elem2','mainmenu elem3'];
$menuelemstring = '';
$elemhtml = $templates['menuentry'];
foreach ($menuelems as $elem) {
    $elemhtml = str_replace('{elem}', $elem, $elemhtml);
    $elemhtml = str_replace('{menuclass}', 'mainmenu', $elemhtml);
    $menuelemstring .= $elemhtml;
}
$templates['home'] = str_replace('{mainmenu}', $menuelemstring, $templates['home']);
/**
 * generate header menu entries from DB
 * and put them in main template
 */
// $sql = "";
// $headermenuelems = dba::prepareExecuteGetStatement($sql);
$headermenuelems = ['headermenu elem1','headermenu elem2'];
$headermenuelemstring = '';
$elemhtml = $templates['menuentry'];
foreach ($headermenuelems as $elem) {

    $elemhtml = str_replace('{elem}', $elem, $elemhtml);
    $elemhtml = str_replace('{menuclass}', 'headermenu', $elemhtml);
    $headermenuelemstring .= $elemhtml;
}
$templates['home'] = str_replace('{headermenu}', $headermenuelemstring, $templates['home']);

/**
 * echo filled template
 */
echo $templates['home'];