CJB Webiste V3 Milestones/ToDo:
1. Mockup Landing Page
2. Add DB access
	Load Background image dynamically
	Load info dynamically
3. Addin Test
4. Mockup Event Preview Addin
5. Add DB reads
	Test include Pseudo SuperEvent
	Test include Pseudo Repetitive Event
---V3.0---
6. Mockup Pictures Addin
7. Add DB reads
---V3.1---
8. Mockup CMS site
9. Add Login (Encrypt myself)
10. Add Permission Layers
11. CMS works?!
---V3.2---
12. Really annoy server guy about SSL-Certificate
13. Facebook API
---V3.2.1---
14. Revive Wiki
---V3.3---
15. Add Dynamic Repetitive EventPage
---V3.4---
16. Add Dynamic SuperEvent Page