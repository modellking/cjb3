<?php
require_once 'helper.php';
/**
 * Created by PhpStorm.
 * User: User
 * Date: 07.06.2017
 * Time: 09:01
 */
class cjb3
{
    private $forcePermissionReload;
    private $perm;


    //<editor-fold desc="User">
    //<editor-fold desc="User: Create">

    /**
     * @param string $new_users_name
     * @param string $new_users_password
     * @param int $new_users_role
     * @param string $username the username of the submitting person
     * @param string $password the password of the submitting person
     * @return bool
     * @throws Exception
     */
    public function createUser(string $new_users_name, string $new_users_password, int $new_users_role = PERMISSIONS_EDITOR,
                               string $username, string $password)
    {
        $perm = $this->getPermissions($username, $password)[0];
        if ($new_users_role >= $perm || $perm < MIN_ALLOW_USER_CREATE) {
            throw new Exception("Insufficient Permissions! you $username have permission level $perm");
        }
        $new_users_password = $this->pepperedPassGen($new_users_password);
        $sql= 'INSERT INTO udvide.Users VALUES (?,?,?,?)';
        access_DB::prepareExecuteFetchStatement($sql,[$new_users_name,false,$new_users_password,$new_users_role]);
        $this->forcePermissionReload = true;
        return true;
    }
    //</editor-fold>

    //<editor-fold desc="User: Read">
    /**
     * @param string $subject username
     * @param string $pass his pw
     * @return array|bool false on invalid login, integer if admin/client or array of targetIds
     */
    public function getPermissions(string $subject, string $pass)
    {
        if (isset($this->perm) && !$this->forcePermissionReload) {
            return $this->perm;
        }

        $sql = <<<'SQL'
SELECT u.passHash, u.deleted, u.role, e.t_id
FROM udvide.Users u
LEFT JOIN Editors e
ON u.username = e.username
LEFT JOIN Targets t
ON e.t_id = t.t_id
WHERE u.username = ?
AND t.deleted = FALSE
SQL;
        $db = access_DB::prepareExecuteFetchStatement($sql, [$subject]); // Don't trust the user!
        if ($db === false) {
            $this->perm = false;
            return false; // user doesn't exist
        }
        if ($db[0]['deleted'] === true) {
            $this->perm = false;
            return false; // user is marked for deletion
        }
        if (!$this->pepperedPassCheck($pass, $db[0]['passHash'])) {
            $this->perm = false;
            return false; // password incorrect
        }
        if ($db[0]['role'] > PERMISSIONS_EDITOR) {
            $this->perm = [$db[0]['role']];
            return [$db[0]['role']]; // returns role if not editor
        }
        $allMarkers = [];
        foreach ($db as $i=>$row) { // since each row is indexed with an integer (starting at 0) $i will just iterate
            $allMarkers[$i] = $row['t_id'];
        }
        $this->perm = [PERMISSIONS_EDITOR,$allMarkers];
        $this->forcePermissionReload = false;
        return [PERMISSIONS_EDITOR,$allMarkers]; // return an array like [1,['tid1','tid2']]
    }
    //</editor-fold>

    //<editor-fold desc="User: Update">

    //</editor-fold>

    //<editor-fold desc="User: Delete">
    /**
     * @param string $user
     * @param string $username
     * @param string $password
     * @return bool
     * @throws PermissionException
     */
    public function deleteUser(string $user, string $username, string $password)
    {
        $perm = $this->getPermissions($username, $password)[0];
        if ($perm === false) {
            throw new PermissionException('Please log in to do that!');
        }
        if ($user !== $username && $perm < MIN_ALLOW_USER_DELETE)
            throw new PermissionException("Insufficient Permissions to delete $user!");

        $sql = 'DELETE FROM udvide.Users WHERE username = ?';
        access_DB::prepareExecuteFetchStatement($sql,[$user]);
        $this->forcePermissionReload = true;
        return true;
    }

    public function deactivateUser(string $user, string $username, string $password)
    {
        $perm = $this->getPermissions($username, $password)[0];
        if ($perm === false) {
            throw new PermissionException('Please log in to do that!');
        }
        if ($user !== $username && $perm < MIN_ALLOW_USER_DEACTIVATE)
            throw new PermissionException("Insufficient Permissions to delete $user!");

        // ToDo updateUser()
        $this->forcePermissionReload = true;
        return true;
    }
    //</editor-fold>
    //</editor-fold>

    /**
     * Compares a sent in passHash (password) with a peppered and salted passHash
     * @param string $userPassHash the sent password value (should be hashed client-side)
     * @param string $serverPassHash the db stored password
     * @return bool
     */
    private function pepperedPassCheck(string $userPassHash,string $serverPassHash):bool
    {
        $keys = json_decode(file_get_contents('keys.json'));
        return password_verify(sha1($userPassHash . $keys->pepper), $serverPassHash);
    }

    /**
     * generates a peppered and salted passHash
     * @param string $new_users_password
     * @return bool|string
     */
    private function pepperedPassGen(string $new_users_password)
    {
        $keys = json_decode(file_get_contents('keys.json'));
        return password_hash(sha1($new_users_password . $keys->pepper), PASSWORD_DEFAULT);
    }
}