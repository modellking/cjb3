<?php
require_once 'cjb3.php';

$root_password = "imGoingToBePepperedAndSalted";
$default_password = "iAmBad";

// manually setup root
$sql = <<<'SQL'
INSERT INTO cjbuffmain.Users ()
VALUES (?,?,?)
SQL;
$keys = json_decode(file_get_contents('keys.json'));
$root_peppered_salted_password = password_hash(sha1($root_password . $keys->pepper),PASSWORD_DEFAULT);
dbaccess::prepareExecuteFetchStatement($sql,['root',$root_peppered_salted_password,PERMISSIONS_ROOT]);
echo "root created! \n<br/>";

// add devs as root
$cu = new cjb3();
$cu->createUser("dev/simon",$default_password,PERMISSIONS_DEV,'root',$root_password);

// add test ppl as root
$cu->createUser("test/tLT", $default_password,PERMISSIONS_LT,'root',$root_password);
$cu->createUser("test/tMAK", $default_password,PERMISSIONS_MAT,'root',$root_password);
$cu->createUser("test/tEditor", $default_password,PERMISSIONS_EDITOR,'root',$root_password);
$cu->createUser("test/tUser", $default_password,PERMISSIONS_USER,'root',$root_password);
echo "devs and test users created!\n<br/>";