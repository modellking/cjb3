<?php
require_once "helper.php";

/**
 * init
 */
$phptime = time();
$nextfridaymidnight = ((($phptime - $phptime % 86400) / 86400) % 7) * 86400 + ($phptime + $phptime % 86400);

$sql = file_get_contents('templates/sql/featuredevent.sql');
// might abstract template behavior implementation once or twice more
// (specify behavior from UI overwritten php or even db)
$sql = str_replace('0/*phptime*/', $phptime, $sql);
$sql = str_replace('0/*nextfridaymidnight*/', $nextfridaymidnight, $sql);
$promotedevent = dbaccess::prepareExecuteFetchStatement($sql);

/**
 * get html templates from file
 * access via $templates['name']
 */
$htmltemplates = json_decode(file_get_contents('templates/html/index.json'));
$templates = [];
foreach ($htmltemplates as $template => $filename) {
    $templates[$template] = file_get_contents('templates/html/' . $filename);
};

/**
 * generate main menu entries from DB
 * and put them in main template
 */
$sql = "";
$menuelems = dbaccess::prepareExecuteFetchStatement($sql);
$menuelemstring = '';
$elemhtml = $templates['menuentry'];
foreach ($menuelems as $elem) {
    $elemhtml = str_replace('{elem}', $elem, $elemhtml);
    $elemhtml = str_replace('{menuclass}', 'mainmenu',$elemhtml);
    $menuelemstring .= $elemhtml;
}
$templates['home'] = str_replace('{mainmenu}', $menuelemstring,$templates['home']);

/**
 * generate header menu entries from DB
 * and put them in main template
 */
$sql = "";
$headermenuelems = dbaccess::prepareExecuteFetchStatement($sql);
$headermenuelemstring = '';
$elemhtml = $templates['menuentry'];
foreach ($headermenuelems as $elem) {
    $elemhtml = str_replace('{elem}', $elem, $elemhtml);
    $elemhtml = str_replace('{menuclass}', 'headermenu', $elemhtml);
    $headermenuelemstring .= $elemhtml;
}
$templates['home'] = str_replace('{headermenu}', $headermenuelemstring, $templates['home']);

/**
 * echo filled template
 */
echo $templates['home'];