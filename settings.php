<?php
// User friendly Settings
define('LANGUAGE','en');

// Where am i?
$host= gethostname();
$ip = gethostbyname($host);
define('EXTERNAL_BASE_PATH',$ip);
define('INTERNAL_BASE_PATH','C:\Users\User\Documents\udvide-Server');

// Where is my DB?
define('SQL_DB_SERVER','localhost');
define('SQL_DB_USERNAME','root');
define('SQL_DB_PASSWORD','');
define('SQL_DB_NAME','udvide');
define('SQL_DB_CHARSET','utf8mb4');

// Who is who?
define('PERMISSIONS_ROOT',6);
define('PERMISSIONS_DEV',5);
define('PERMISSIONS_LT',4);
define('PERMISSIONS_MAT',3);
define('PERMISSIONS_EDITOR',2);
define('PERMISSIONS_USER',1);
define('PERMISSIONS_ANYONE',0);

// Who is allowed to do what?
//  Users
define('MIN_ALLOW_USER_CREATE',PERMISSIONS_ANYONE);
define('MIN_ALLOW_USER_UPDATE',PERMISSIONS_MAT); //ToDo
define('ALLOW_SELF_UPDATE',true); //ToDo
define('MIN_ALLOW_USER_DEACTIVATE',PERMISSIONS_MAT); //ToDo
define('ALLOW_SELF_DEACTIVATE',true); //ToDo
define('MIN_ALLOW_USER_DELETE',PERMISSIONS_DEV);
define('ALLOW_SELF_DELETE',false); //ToDo

// What do others expect from us?

// Internal Settings
// Error codes ToDo
$errLang = json_decode(file_get_contents('errLang_'.LANGUAGE.'.json'));
foreach ($errLang as $key=>$value) {
    define('ERR_' . $key, $value);
}
// Debug var dumps
define('DEBUG_ACCESS_DB',false);

define('DS', DIRECTORY_SEPARATOR);
