/**
 * Target:
 * Get following Events:
 * Slot 1: next Superevent (Events gehören hier hinein, wenn sie selbst aus mehreren Anderen Events bestehen z.B. Zeltmission)
 * Slot 2: all Superevents children orderd by starttime
 * Slot 3: all yearly events within 6 months ordered by priority (Freizeiten, etc.)
 * Slot 4: all next ~monthly event ordered by priority
 * Slot 5: next Friday
 * Ordered by:
 * If S1 was created within 2 weeks or (happens within 2 months and not more than 1 week) 1,2,3,4,5
 * If S1 happens within next week: 2,1,3,4,5
 * If S3 happens within 2 weeks: 3,1,5,4
 * Else 5,1,3,4
 * After:
 * If S5 == S4 show only first of both
 * If S5 == S3 show only first of both
 *
 */
SELECT DISTINCT eventID, name, starttime, description, fbPath
FROM (
        (/* SLOT1 next superevent*/
          SELECT
            1 slot,
            eventID, name, starttime, description, fbPath, priority
          FROM Events
          WHERE starttime > 0/*phptime*/
                AND minslot = 1
          ORDER BY starttime
          LIMIT 1
        )
      UNION ALL
        (/* SLOT2 all SLOT1 children orderd by starttime*/
          SELECT
            2 slot,
            e.eventID, name, starttime, description, fbPath, priority
          FROM Events e
            LEFT JOIN (
                        SELECT eventID
                        FROM Events
                        ORDER BY starttime
                        LIMIT 1
                      ) se
              ON se.eventID = e.superEvent
          WHERE starttime > 0/*phptime*/
                AND minslot = 2
          ORDER BY starttime
        )
      UNION ALL
        (/* SLOT3 all yearly events within 6 months ordered by priority (Freizeiten, etc.) */
          SELECT
            3 slot,
            eventID, name, starttime, description, fbPath, priority
          FROM Events
          WHERE starttime > 0/*phptime*/
                AND starttime < 0/*phptime*/ + 15768000
                AND minslot = 3
          ORDER BY priority
        )
      UNION ALL
        (/* SLOT4 all next ~monthly event ordered by priority */
          SELECT
            4 slot,
            eventID, name, starttime, description, fbPath, priority
          FROM Events
          WHERE starttime > 0/*phptime*/
                AND starttime < 0/*phptime*/ + 15768000
                AND minslot = 4
          ORDER BY priority
        )
      UNION ALL
        (/* SLOT5 next friday if its not special */
          SELECT
            5 slot,
            eventID, name, starttime, description, fbPath, priority
          FROM Events
          WHERE starttime > 0/*phptime*/
                AND starttime < 0/*nextfridaymidnight*/
                AND minslot = 5
          ORDER BY priority
        )
    ) canditates
ORDER BY canditates.slot
LIMIT 5
